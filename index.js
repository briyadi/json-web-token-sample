const express = require("express")
const app = express()
const SECRET = "s3cr3t"
const jwt = require("jsonwebtoken")

const dataUser = {
    name: "Sabrina",
    age: 25
}

app.use(express.json())
let protectedPage = (req, res, next) => {
    // pengecekan token apakah valid atau tidak
    try {
        let token = req.headers.authorization.split("Bearer ")[1]
        let user = jwt.verify(token, SECRET)
        if (!user) {
            throw new Error("invalid token")
        }

        req.user = user
    } catch (e) {
        return res.status(401).json({
            message: "invalid token"
        })
    }

    // jalankan handler / controller selanjutnya
    next()
}

app.get("/profile", protectedPage, (req, res) => {
    res.status(200).json(req.user)
})

// login endpoint
app.post("/login", (req, res) => {
    if (req.body.username == 'admin' && req.body.password == 'admin') {
        let token = jwt.sign(dataUser, SECRET)

        return res.status(200).json({
            token: token
        })
    }

    return res.status(401).json({message: "invalid username or password"})
})

app.listen(8000, () => {
    console.log(`server running on port 8000`)
})